#! /usr/bin/env python3

from PIL import Image
import numpy as np
import math
from random import randrange


def main():
    source = Image.open("testimg.png").convert("L")

    w, h = source.size

    canvas = source.copy()
    pixel = canvas.load()

    #pixel[500,500] = 0
    A = 0
    B = 255
    th = math.floor((B - A)/2)

    print("Threshholding --")

    for y in range(0, h):
        print("\r %6.2f%% " % (y / h * 100), end='')

        for x in range(0, w):
            if pixel[x,y] > th:
                pixel[x,y] = B
            else:
                pixel[x,y] = A

    canvas.save("testimg_th.png")

    print("\r 100.00%%")
    
    canvas = source.copy()
    pixel = canvas.load()
    
    print("random --")

    for y in range(0, h):
        print("\r %6.2f%% " % (y / h * 100), end='')

        for x in range(0, w):
            if pixel[x,y] > randrange(0, 255):
                pixel[x,y] = B
            else:
                pixel[x,y] = A

    canvas.save("testimg_r.png")

    print("\r 100.00%%")

    canvas = source.copy()
    pixel = canvas.load()

    print("Floyd-Steinberg --")

    # Floyd-Steinberg kernel:
    #
    # 1/16 . |   | * | 7 |
    #        | 3 | 5 | 1 |

    k = np.array([ [ 0, 0, 7 ],
                   [ 3, 5, 1 ] ]) / 16.

    k = k.transpose() # k[x][y]

    for y in range(0, 0):
        print("\r %6.2f%% " % (y / h * 100), end='')

        for x in range(0, w):
            if pixel[x,y] > th:
                err = pixel[x,y] - B
                pixel[x,y] = B
            else:
                err = pixel[x,y] - A
                pixel[x,y] = A

            if x+1 < w:
                pixel[ x+1, y   ] = pixel[ x+1, y   ] + int(round(err * k[2][0]))

            if y+1 < h:
                if x-1 >= 0:
                    pixel[ x-1, y+1 ] = pixel[ x-1, y+1 ] + int(round(err * k[0][1]))

                pixel[ x,   y+1 ] = pixel[ x,   y+1 ] + int(round(err * k[1][1]))

                if x+1 < w:
                    pixel[ x+1, y+1 ] = pixel[ x+1, y+1 ] + int(round(err * k[2][1]))

    canvas.save("testimg_fs.png")

    print("\r 100.00%%")

    canvas = source.copy()
    pixel = canvas.load()

    print("Floyd-Steinberg Serpentine")

    for y in range(0, 0):
        print("\r %6.2f%% " % (y / h * 100), end='')

        if y % 2 > 0:
            for i in range(0, int(w/2)):
                temp = pixel[ i, y ]
                pixel[ i, y ] = pixel[ w-1-i, y ]
                pixel[ w-1-i, y ] = temp

                if y+1 < h:
                    temp = pixel[ i, y+1 ]
                    pixel[ i, y+1 ] = pixel[ w-1-i, y+1 ]
                    pixel[ w-1-i, y+1 ] = temp

        for x in range(0, w):
            if pixel[x,y] > th:
                err = pixel[x,y] - B
                pixel[x,y] = B
            else:
                err = pixel[x,y] - A
                pixel[x,y] = A

            if x+1 < w:
                pixel[ x+1, y   ] = pixel[ x+1, y   ] + int(round(err * k[2][0]))

            if y+1 < h:
                if x-1 >= 0:
                    pixel[ x-1, y+1 ] = pixel[ x-1, y+1 ] + int(round(err * k[0][1]))

                pixel[ x,   y+1 ] = pixel[ x,   y+1 ] + int(round(err * k[1][1]))

                if x+1 < w:
                    pixel[ x+1, y+1 ] = pixel[ x+1, y+1 ] + int(round(err * k[2][1]))

        if y % 2 > 0:
            for i in range(0, int(w/2)):
                temp = pixel[ i, y ]
                pixel[ i, y ] = pixel[ w-1-i, y ]
                pixel[ w-1-i, y ] = temp

                if y+1 < h:
                    temp = pixel[ i, y+1 ]
                    pixel[ i, y+1 ] = pixel[ w-1-i, y+1 ]
                    pixel[ w-1-i, y+1 ] = temp

    canvas.save("testimg_fs_s.png")

    print("\r 100.00%%")

    canvas = source.copy()
    pixel = canvas.load()

    print("Jarvis-Judice-Ninke --")

    # Jarvis-Judice-Ninke kernel:
    #
    #        |   |   | * | 7 | 5 |
    # 1/48 . | 3 | 5 | 7 | 5 | 3 |
    #        | 1 | 3 | 5 | 3 | 1 |

    k = np.array([ [ 0, 0, 0, 7, 5 ],
                   [ 3, 5, 7, 5, 3 ],
                   [ 1, 3, 5, 3, 1 ] ]) / 48.

    k = k.transpose() # k[x][y]

    for y in range(0, h):
        print("\r %6.2f%% " % (y / h * 100), end='')

        for x in range(0, w):
            if pixel[x,y] > th:
                err = pixel[x,y] - B
                pixel[x,y] = B
            else:
                err = pixel[x,y] - A
                pixel[x,y] = A

            if x+1 < w:
                pixel[ x+1, y   ] = pixel[ x+1, y   ] + int(round(err * k[3][0]))
                if x+2 < w:
                    pixel[ x+2, y   ] = pixel[ x+2, y   ] + int(round(err * k[4][0]))

            if y+1 < h:
                pixel[ x,   y+1 ] = pixel[ x,   y+1 ] + int(round(err * k[2][1]))
                if x-1 > 0:
                    pixel[ x-1, y+1 ] = pixel[ x-1, y+1 ] + int(round(err * k[1][1]))
                    if x-2 > 0:
                        pixel[ x-2, y+1 ] = pixel[ x-2, y+1 ] + int(round(err * k[0][1]))
                
                if x+1 < w:
                    pixel[ x+1, y+1 ] = pixel[ x+1, y+1 ] + int(round(err * k[3][1]))
                    if x+2 < w:
                        pixel[ x+2, y+1 ] = pixel[ x+2, y+1 ] + int(round(err * k[4][1]))

                if y+2 < h:
                    pixel[ x,   y+2 ] = pixel[ x,   y+2 ] + int(round(err * k[2][2]))
                    if x-1 > 0:
                        pixel[ x-1, y+2 ] = pixel[ x-1, y+2 ] + int(round(err * k[1][2]))
                        if x-2 > 0:
                            pixel[ x-2, y+2 ] = pixel[ x-2, y+2 ] + int(round(err * k[0][2]))
                    
                    if x+1 < w:
                        pixel[ x+1, y+2 ] = pixel[ x+1, y+2 ] + int(round(err * k[3][2]))
                        if x+2 < w:
                            pixel[ x+2, y+2 ] = pixel[ x+2, y+2 ] + int(round(err * k[4][2]))

    canvas.save("testimg_jjn.png")

    print("\r 100.00%%")

    canvas = source.copy()
    pixel = canvas.load()

    print("Jarvis-Judice-Ninke Serpentine --")
    print(" === REDACTED ===")
    # it'd seem serpentine JJN is horrible and doesn't work at all
    """
    for y in range(2, h-2):
        print("\r %6.2f%% " % (y / h * 100), end='')

        if y % 2 > 0:
            for i in range(0, int(w/2)):
                temp = pixel[ i, y ]
                pixel[ i, y ] = pixel[ w-1-i, y ]
                pixel[ w-1-i, y ] = temp

                if y+1 < h:
                    temp = pixel[ i, y+1 ]
                    pixel[ i, y+1 ] = pixel[ w-1-i, y+1 ]
                    pixel[ w-1-i, y+1 ] = temp

        for x in range(2, w-2):
            if pixel[x,y] > th:
                err = pixel[x,y] - B
                pixel[x,y] = B
            else:
                err = pixel[x,y] - A
                pixel[x,y] = A

            #if x+1 < w:
            pixel[ x+1, y   ] = pixel[ x+1, y   ] + int(round(err * k[3][0]))
            pixel[ x+2, y   ] = pixel[ x+2, y   ] + int(round(err * k[4][0]))

            #if y+1 < h:
                #if x-1 >= 0:
            pixel[ x-2, y+1 ] = pixel[ x-2, y+1 ] + int(round(err * k[0][1]))
            pixel[ x-1, y+1 ] = pixel[ x-1, y+1 ] + int(round(err * k[1][1]))
            pixel[ x,   y+1 ] = pixel[ x,   y+1 ] + int(round(err * k[2][1]))
            pixel[ x+1, y+1 ] = pixel[ x+1, y+1 ] + int(round(err * k[3][1]))
            pixel[ x+2, y+1 ] = pixel[ x+2, y+1 ] + int(round(err * k[4][1]))

                #if x+1 < w:

            pixel[ x-2, y+2 ] = pixel[ x-2, y+2 ] + int(round(err * k[0][2]))
            pixel[ x-1, y+2 ] = pixel[ x-1, y+2 ] + int(round(err * k[1][2]))
            pixel[ x,   y+2 ] = pixel[ x,   y+2 ] + int(round(err * k[2][2]))
            pixel[ x+1, y+2 ] = pixel[ x+1, y+2 ] + int(round(err * k[3][2]))
            pixel[ x+2, y+2 ] = pixel[ x+2, y+2 ] + int(round(err * k[4][2]))

        if y % 2 > 0:
            for i in range(0, int(w/2)):
                temp = pixel[ i, y ]
                pixel[ i, y ] = pixel[ w-1-i, y ]
                pixel[ w-1-i, y ] = temp

                if y+1 < h:
                    temp = pixel[ i, y+1 ]
                    pixel[ i, y+1 ] = pixel[ w-1-i, y+1 ]
                    pixel[ w-1-i, y+1 ] = temp

    canvas.save("testimg_jjn_s.png")

    print("\r 100.00%%")
    """
    canvas = source.copy()
    pixel = canvas.load()

    print("Ostromoukhov --")


    print("Not implemented:")
    print(" - Stucki")
    print(" - Ordered (Bayer)")
    print(" - clustered dot")
    print(" - optimum kernel")
    print(" - Shiau and Fan")
    print(" - \"Standard Error Diffusion\" Zhou and Fang")
    print(" - \"Structure Aware Halftoning\" Pang et al.")

if __name__ == "__main__":
    main()
